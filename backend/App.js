const express = require('express');
const { Driver, getCredentialsFromEnv, TableDescription, Column, Types, getLogger } = require('ydb-sdk');
const dotenv = require('dotenv');
const { v4: uuidv4 } = require('uuid');
dotenv.config();

const app = express();
const port = process.env.PORT || 10000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const authService = new getCredentialsFromEnv();

const ydbDriver = new Driver({
  endpoint: process.env.YDB_ENDPOINT,
  database: process.env.YDB_DATABASE,
  authService,
  logger: getLogger({ level: 'debug' }),
  timeout: 30000
});

ydbDriver.ready().then(async () => {
  console.log('Connected to YDB');
  await initDatabase();
}).catch(err => {
  console.error('Failed to connect to YDB', err);
  process.exit(1);
});

async function initDatabase() {
  try {
    await ydbDriver.tableClient.withSession(async (session) => {
    console.log("Start initializing table");
      const tableDescription = new TableDescription()
        .withColumns(
          new Column('id', Types.optional(Types.UTF8)),
          new Column('message', Types.optional(Types.UTF8))
        )
        .withPrimaryKey('id');

      await session.createTable('guestbook', tableDescription);
    }, 50000);
    console.log("Table initialized");
  } catch (err) {
    console.error("Failed to initialize database", err);
    throw err;
  }
}

app.get('/api/messages', async (req, res) => {
  try {
    const result = await ydbDriver.tableClient.withSession(async (session) => {
      const query = 'SELECT id, message FROM guestbook';
      const resultSet = await session.executeQuery(query);
      return resultSet;
    });
    res.setHeader('Content-Type', 'application/json');
    res.json(result.resultSets[0].rows);
  } catch (err) {
    console.error("Failed to get messages", err);
    res.status(500).send("Internal Server Error");
  }
});

app.post('/api/messages', async (req, res) => {
  try {
    const id = uuidv4();
    const { message } = req.body;
    await ydbDriver.tableClient.withSession(async (session) => {
      const query = `UPSERT INTO guestbook (id, message) VALUES ("${id}", "${message}")`;
      await session.executeQuery(query);
    });
    res.setHeader('Content-Type', 'application/json');
    res.status(201).send('Message added');
  } catch (err) {
    console.error("Failed to add message", err);
    res.status(500).send("Internal Server Error");
  }
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
