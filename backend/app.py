import json
import os
import flask
from flask import Flask, request, jsonify
import ydb
from ydb import Driver, Column, TableDescription
from ydb.iam import ServiceAccountCredentials
from dotenv import load_dotenv
from uuid import uuid4
from flask_cors import CORS

load_dotenv()

app = Flask(__name__)
CORS(app)

ydb_endpoint = os.getenv('YDB_ENDPOINT')
ydb_database = os.getenv('YDB_DATABASE')
service_account_key_file = os.getenv('YDB_SERVICE_ACCOUNT_KEY_FILE_CREDENTIALS')

driver_config = ydb.DriverConfig(ydb_endpoint, ydb_database, credentials=ServiceAccountCredentials.from_file(service_account_key_file))
driver = Driver(driver_config)

@app.route('/api/messages', methods=['GET'])
def get_messages():
    try:
        session = driver.table_client.session().create()
        result = session.transaction(ydb.SerializableReadWrite()).execute(
            'SELECT id, message FROM guestbook',
            commit_tx=True
        )
        messages = [{'id': row['id'], 'message': row['message']} for row in result[0].rows]
        return jsonify(messages)
    except Exception as e:
        print(e)
        return jsonify({'error': str(e)}), 500

@app.route('/api/messages', methods=['POST'])
def add_message():
    try:
        data = json.loads(request.get_data())
        message = data['message']
        message_id = str(uuid4())

        session = driver.table_client.session().create()
        session.transaction().execute(
            f'UPSERT INTO guestbook (id, message) VALUES ("{message_id}", "{message}")', commit_tx=True
        )
        return jsonify({'message': 'Message added'}), 201
    except Exception as e:
        print(e)
        return jsonify({'error': str(e)}), 500

@app.route('/api/info', methods=['GET'])
def get_info():
    try:
        version = "1.0.0"
        replica = os.uname()[1]
        return jsonify({'version': version, 'replica': replica})
    except Exception as e:
        print(e)
        return jsonify({'error': str(e)}), 500

@app.route('/health', methods=['GET'])
def health_check():
    return jsonify({'status': 'healthy'})

@app.route('/api/messages', methods=['OPTIONS'])
@app.route('/api/info', methods=['OPTIONS'])
def options():
    resp = flask.Response("")
    resp.headers["Access-Control-Allow-Origin"] = "*"
    resp.headers["Access-Control-Allow-Methods"] = 'GET, POST, OPTIONS'
    resp.headers["Access-Control-Allow-Headers"] = 'Content-Type'
    return jsonify()

def init_database():
    try:
        session = driver.table_client.session().create()
        session.create_table(
            f"{ydb_database}/guestbook",
            ydb.TableDescription()
            .with_column(ydb.Column('id', ydb.PrimitiveType.Utf8))
            .with_column(ydb.Column('message', ydb.OptionalType(ydb.PrimitiveType.Utf8)))
            .with_primary_key('id')
        )
        print("Table initialized")
    except Exception as e:
        print(f"Failed to initialize database: {e}")

if __name__ == '__main__':
    driver.wait(timeout=30)
    init_database()
    app.run(host='0.0.0.0', port=80)
