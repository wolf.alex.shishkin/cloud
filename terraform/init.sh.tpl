#!/bin/bash

sudo apt-get update
sudo apt-get install -y python3-pip python3-venv git

USERNAME="ubuntu"
REPO_URL=${git_repo_url}
REPO_DIR="/home/$USERNAME/repo"
sudo git clone $REPO_URL $REPO_DIR

cd $REPO_DIR/backend
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt

FULL_ENDPOINT=${ydb_full_endpoint}
YDB_ENDPOINT=$(echo $FULL_ENDPOINT | grep -o 'grpcs://[^/]*/' | sed 's/.$//')
YDB_DATABASE=$(echo $FULL_ENDPOINT | grep -o '/[^?]*$')

cat <<EOF | sudo tee /home/$USERNAME/.ydb_sa_key.json
${ydb_service_account_key}
EOF

cat <<EOF | sudo tee $REPO_DIR/backend/.env
YDB_ENDPOINT=$${YDB_ENDPOINT}
YDB_DATABASE=$${YDB_DATABASE}
YDB_SERVICE_ACCOUNT_KEY_FILE_CREDENTIALS=/home/$USERNAME/.ydb_sa_key.json
EOF

sudo /home/$USERNAME/repo/backend/venv/bin/python /home/$USERNAME/repo/backend/app.py &

cat <<EOF | sudo tee /etc/systemd/system/guestbook.service
[Unit]
Description=guestbook app

[Service]
ExecStart=/home/$USERNAME/repo/backend/venv/bin/python /home/$USERNAME/repo/backend/app.py
Restart=always
User=$USERNAME
Group=$USERNAME
WorkingDirectory=$REPO_DIR/backend

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl daemon-reload
sudo systemctl enable guestbook
sudo systemctl start guestbook

echo "Python server is running and configured to start on boot."
