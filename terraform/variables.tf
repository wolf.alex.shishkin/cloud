variable "zone" {
  type    = string
  default = "ru-central1-a"
}

variable "network" {
  type    = string
  default = "ya-network"
}

variable "subnet" {
  type    = string
  default = "ya-subnet"
}

variable "subnet_v4_cidr_blocks" {
  type    = list(string)
  default = ["192.168.10.0/24"]
}

variable "image_family" {
  type    = string
  default = "ubuntu-2004-lts"
}

variable "name" {
  type    = string
}

variable "replica_count" {
  type    = number
  default = 2
}

variable "timeout_create" {
  default = "10m"
}

variable "timeout_delete" {
  default = "10m"
}

variable "ssh_public_key" {
  type = string
}

variable "service_account_key_file" {
  type      = string
  sensitive = true
}

variable "static_key_pub" {
  type      = string
  sensitive = true
}

variable "static_key_secret" {
  type      = string
  sensitive = true
}

variable "folder_id" {
  type = string
}

variable "bucket_name" {
  type = string
}

variable "service_account_id" {
  type = string
}

variable "git_repo_url" {
  type = string
}
