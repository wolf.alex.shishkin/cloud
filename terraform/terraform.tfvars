zone                     = "ru-central1-a"
network                  = "ya-network"
subnet                   = "ya-subnet"
subnet_v4_cidr_blocks    = ["192.168.10.0/24"]
name                     = "guestbook-backend"
image_family             = "ubuntu-2004-lts"
replica_count            = 2
cores                    = 2
memory                   = 4
disk_size                = 50
disk_type                = "network-ssd"
nat                      = true
timeout_create           = "10m"
timeout_delete           = "10m"
git_repo_url = "https://gitlab.com/wolf.alex.shishkin/cloud.git"
bucket_name = "pohotlivi-guest-book"