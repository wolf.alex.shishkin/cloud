terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}

provider "yandex" {
  zone                     = var.zone
  folder_id                = var.folder_id
  service_account_key_file = var.service_account_key_file
  storage_access_key = var.static_key_pub
  storage_secret_key = var.static_key_secret
}

resource "yandex_vpc_network" "default" {
  name = var.network
}

resource "yandex_vpc_subnet" "default" {
  network_id     = yandex_vpc_network.default.id
  name           = var.subnet
  v4_cidr_blocks = var.subnet_v4_cidr_blocks
  zone           = var.zone
}

data "yandex_compute_image" "default" {
  family = var.image_family
}

resource "yandex_iam_service_account" "default" {
  name        = "guestbook-sa"
  description = "Service account for Guestbook application"
}

resource "yandex_iam_service_account_static_access_key" "default" {
  service_account_id = yandex_iam_service_account.default.id
  description        = "Static access key for YDB"
}

resource "yandex_iam_service_account_key" "default" {
  service_account_id = yandex_iam_service_account.default.id
  description        = "Service account key for Guestbook"
}

output "ydb_service_account_key" {
  sensitive = true
  value     = yandex_iam_service_account_key.default.private_key
}

resource "yandex_ydb_database_serverless" "default" {
  name       = "guestbook-db"
  folder_id  = var.folder_id
}

resource "yandex_ydb_database_iam_binding" "admin" {
  database_id = yandex_ydb_database_serverless.default.id
  role   = "ydb.admin"
  members = [
    "serviceAccount:${var.service_account_id}",
    "serviceAccount:${yandex_iam_service_account.default.id}"
  ]
}

data "template_file" "sa_key_json" {
  template = file("${path.module}/sa_key.tpl.json")
  vars = {
    id                  = yandex_iam_service_account_key.default.id
    service_account_id  = yandex_iam_service_account_key.default.service_account_id
    created_at          = yandex_iam_service_account_key.default.created_at
    key_algorithm       = yandex_iam_service_account_key.default.key_algorithm
    public_key          = replace(yandex_iam_service_account_key.default.public_key, "\n", "\\n")
    private_key         = replace(yandex_iam_service_account_key.default.private_key, "\n", "\\n")
  }
}

resource "local_file" "sa_key" {
  content  = data.template_file.sa_key_json.rendered
  filename = "${path.module}/sa_key.json"
}

output "ydb_endpoint" {
  value = yandex_ydb_database_serverless.default.ydb_full_endpoint
}

output "ydb_database" {
  value = yandex_ydb_database_serverless.default.name
}

data "template_file" "init" {
  template = file("${path.module}/init.sh.tpl")
  vars     = {
    git_repo_url          = var.git_repo_url,
    ydb_full_endpoint     = yandex_ydb_database_serverless.default.ydb_full_endpoint,
    ydb_service_account_key = local_file.sa_key.content,
  }
}

resource "yandex_compute_instance" "default" {
  count = var.replica_count

  name     = "${var.name}-${count.index}"
  hostname = "${var.name}-${count.index}"
  zone     = var.zone

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.default.id
      size     = 20
      type     = "network-hdd"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.default.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${var.ssh_public_key}"
    user-data = data.template_file.init.rendered
  }

  timeouts {
    create = var.timeout_create
    delete = var.timeout_delete
  }
}

resource "yandex_lb_target_group" "default" {
  name      = "target-group"
  region_id = "ru-central1"

  dynamic "target" {
    for_each = yandex_compute_instance.default
    content {
      address   = target.value.network_interface[0].ip_address
      subnet_id = yandex_vpc_subnet.default.id
    }
  }
}

resource "yandex_lb_network_load_balancer" "default" {
  name      = "network-load-balancer"
  region_id = "ru-central1"
  type      = "external"

  listener {
    name        = "http-listener"
    port        = 80
    target_port = 80

    external_address_spec {}
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.default.id
    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = "/health"
      }
    }
  }
}

resource "yandex_api_gateway" "guestbook_api" {
  name        = "guestbook-api"
  description = "API Gateway for Guestbook application"
  spec = <<-EOF
openapi: 3.0.0
info:
  title: Guestbook API Gateway
  version: 1.0.0
servers:
- url: https://d5djqrlv0ql90v7oi4uk.apigw.yandexcloud.net
paths:
  /api/messages:
    get:
      summary: Get all messages
      description: Retrieve a list of all messages in the guestbook.
      x-yc-apigateway-integration:
        type: http
        url: http://${flatten([for listener in yandex_lb_network_load_balancer.default.listener : [for spec in listener.external_address_spec : spec.address]])[0]}/api/messages
      responses:
        '200':
          description: A list of messages
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: string
                      example: 123e4567-e89b-12d3-a456-426614174000
                    message:
                      type: string
                      example: Hello, world!
    post:
      summary: Create a new message
      description: Add a new message to the guestbook.
      x-yc-apigateway-integration:
        type: http
        url: http://${flatten([for listener in yandex_lb_network_load_balancer.default.listener : [for spec in listener.external_address_spec : spec.address]])[0]}/api/messages
      requestBody:
        description: Message to create
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                message:
                  type: string
                  example: Hello, world!
      responses:
        '201':
          description: Message created successfully
        '400':
          description: Invalid input
    options:
      summary: CORS support
      x-yc-apigateway-integration:
        type: dummy
        http_code: 200
        content: {}
      responses:
        '200':
          description: CORS support
          headers:
            Access-Control-Allow-Origin:
              schema:
                type: string
              example: '*'
            Access-Control-Allow-Methods:
              schema:
                type: string
              example: 'GET, POST, OPTIONS'
            Access-Control-Allow-Headers:
              schema:
                type: string
              example: 'Content-Type'
  /api/info:
    get:
      summary: Get backend info
      description: Retrieve version of the backend and replica name.
      x-yc-apigateway-integration:
        type: http
        url: http://${flatten([for listener in yandex_lb_network_load_balancer.default.listener : [for spec in listener.external_address_spec : spec.address]])[0]}/api/info
      responses:
        '200':
          description: Backend info
          content:
            application/json:
              schema:
                type: object
                properties:
                  version:
                    type: string
                    example: 1.0.0
                  replica:
                    type: string
                    example: replica-1
    options:
      summary: CORS support
      x-yc-apigateway-integration:
        type: dummy
        http_code: 200
        content: {}
      responses:
        '200':
          description: CORS support
          headers:
            Access-Control-Allow-Origin:
              schema:
                type: string
              example: '*'
            Access-Control-Allow-Methods:
              schema:
                type: string
              example: 'GET, POST, OPTIONS'
            Access-Control-Allow-Headers:
              schema:
                type: string
              example: 'Content-Type'
  /health:
    get:
      summary: Health check
      description: Check the health of the backend service.
      x-yc-apigateway-integration:
        type: http
        url: http://${flatten([for listener in yandex_lb_network_load_balancer.default.listener : [for spec in listener.external_address_spec : spec.address]])[0]}/health
      responses:
        '200':
          description: Service is healthy
        '503':
          description: Service is unavailable
  /:
    get:
      summary: Static frontend
      description: Serve the static frontend from Yandex Object Storage.
      x-yc-apigateway-integration:
        type: object_storage
        bucket: pohotlivi-guest-book
        object: index.html
      responses:
        '200':
          description: Static frontend
components:
  schemas:
    Message:
      type: object
      properties:
        id:
          type: string
          example: 123e4567-e89b-12d3-a456-426614174000
        message:
          type: string
          example: Hello, world!
    BackendInfo:
      type: object
      properties:
        version:
          type: string
          example: 1.0.0
        replica:
          type: string
          example: replica-1
EOF
}

data "template_file" "index_html" {
  template = file("${path.module}/../frontend/index.html.tpl")
  vars = {
    api_url = "https://${yandex_api_gateway.guestbook_api.domain}/api"
  }
}

resource "local_file" "processed_index_html" {
  content  = data.template_file.index_html.rendered
  filename = "${path.module}/processed_index.html"
}

resource "yandex_storage_bucket" "frontend_bucket" {
  bucket = "pohotlivi-guest-book"
  acl    = "public-read"
  website {
    index_document = "index.html"
  }
}

resource "yandex_storage_object" "index_html" {
  bucket = yandex_storage_bucket.frontend_bucket.bucket
  key    = "index.html"
  source = local_file.processed_index_html.filename
  content_type = "text/html"
  acl    = "public-read"
}

output "load_balancer_ip" {
  value = flatten([for listener in yandex_lb_network_load_balancer.default.listener : [for spec in listener.external_address_spec : spec.address]])[0]
}

output "instance_public_ips" {
  value = [for instance in yandex_compute_instance.default : instance.network_interface[0].nat_ip_address]
}

output "instance_names" {
  value = [for instance in yandex_compute_instance.default : instance.name]
}

output "api_gateway_url" {
  value = yandex_api_gateway.guestbook_api.domain
}
