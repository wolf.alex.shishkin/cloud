**Предварительные требования**
- Установленный Terraform.
- Установленный AWS CLI и настроенный для работы с Yandex Object Storage.
- Yandex Cloud CLI.

**ДЕПЛОЙ (если вдруг захочется, можно в любой пустой каталог)**
1. Настроить Yandex Cloud CLI, привязать туда сервисный аккаунт
2. Запустить скрипт **configure.py** (там вроде все +- понятно)
3. перейти в папку **terraform** и запустить
    ```powershell
    terraform init
    terraform apply -var-file="terraform.tfvars" -var-file="sensitive.tfvars"
    ```
4. ???
5. profit

**CHANGE_COUNT**
```powershell
./change_count.ps1 add 1337 
./change_count.ps1 remove 228 
```

**UPDATE_BACK**
```powershell
.\update_back.ps1
    Available instances:
    guestbook-backend-0: 158.160.58.156
    guestbook-backend-1: 51.250.76.69

.\update_back.ps1 -InstanceIp "192.168.0.1" [-CommitSha "commit_sha"]
```

**UPDATE_FRONT**
```powershell
.\update_front.ps1 -- загружает текущую версию файла index.html.tpl с подставленным туда API_URL
```

**А ГДЕ СМОТРЕТЬ?**
```powershell
.\gimme_link.ps1
```

**А ЧТО СМОТРЕТЬ?**

Версия бека показывается, реплики меняются (но не каждый раз), сообщения отправляются
