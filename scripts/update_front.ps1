function Get-ApiUrl {
    Set-Location -Path ..\terraform
    $api_url = terraform output -raw api_gateway_url
    Set-Location -Path ..\scripts
    return $api_url
}

function Update-Frontend {
    param (
        [string]$ApiUrl
    )

    $templatePath = "..\frontend\index.html.tpl"
    $outputPath = "..\frontend\index.html"

    $content = Get-Content -Path $templatePath -Raw

    $content = $content -replace "\$\{api_url\}", $ApiUrl

    Set-Content -Path $outputPath -Value $content

    $bucketName = "pohotlivi-guest-book"
    $s3Key = "index.html"
    aws s3 cp $outputPath "s3://$bucketName/$s3Key" --acl public-read
}

$api_url = Get-ApiUrl
Update-Frontend -ApiUrl $api_url
