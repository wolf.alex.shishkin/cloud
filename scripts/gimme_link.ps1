Set-Location -Path ..\terraform
$api_url = terraform output -raw api_gateway_url
Set-Location -Path ..\scripts
echo "https://$api_url/"
