function Get-ReplicaCount {
  $instance_ips = terraform output -json instance_ips
  $ips = $instance_ips | ConvertFrom-Json
  return $ips.Length
}

function Add-Replicas {
  param (
    [int]$CountToAdd
  )

  $currentCount = Get-ReplicaCount
  $newCount = $currentCount + $CountToAdd

  terraform apply -var-file="terraform.tfvars" -var-file="sensitive.tfvars" -var "replica_count=$newCount"
}

function Remove-Replicas {
  param (
    [int]$CountToRemove
  )

  $currentCount = Get-ReplicaCount
  $newCount = $currentCount - $CountToRemove

  if ($newCount -lt 0) {
    Write-Host "Error: Cannot have negative number of replicas."
    exit 1
  }

  terraform apply -var-file="terraform.tfvars" -var-file="sensitive.tfvars" -var "replica_count=$newCount"
}

if ($args.Length -ne 2) {
  Write-Host "Usage: .\script.ps1 add|remove <count>"
  exit 1
}

Set-Location -Path ..\terraform

switch ($args[0]) {
  "add" { Add-Replicas -CountToAdd $args[1] }
  "remove" { Remove-Replicas -CountToRemove $args[1] }
  default { Write-Host "Invalid action. Use 'add' or 'remove'." }
}

Set-Location -Path ..\scripts
