Param (
    [string]$InstanceIp,
    [string]$CommitSha
)

function Get-InstanceIps {
    $instance_ips = terraform output -json instance_public_ips
    return $instance_ips | ConvertFrom-Json
}

function Get-InstanceNames {
    $instance_names = terraform output -json instance_names
    return $instance_names | ConvertFrom-Json
}

function List-Instances {
    $ips = Get-InstanceIps
    $names = Get-InstanceNames
    for ($i = 0; $i -lt $ips.Length; $i++) {
        Write-Host "$($names[$i]): $($ips[$i])"
    }
}

function Update-Backend {
    param (
        [string]$InstanceIp,
        [string]$CommitSha
    )

    if (-not $InstanceIp) {
        Write-Host "Available instances:"
        List-Instances
        return
    }

    $scriptContent = @"
cd /home/ubuntu/repo
sudo git config --global --add safe.directory /home/ubuntu/repo
sudo git fetch origin

"@

    if ($CommitSha) {
        $scriptContent += @"
sudo git checkout $CommitSha`n
"@
    } else {
        $scriptContent += @"
sudo git pull origin`n
"@
    }

    $scriptContent += @"
cd backend
source venv/bin/activate
sudo pip install -r requirements.txt
sudo venv/bin/pip install flask_cors
sudo pkill -9 -f app.py
nohup sudo venv/bin/python app.py > app.out 2>&1 &
exit
"@

    $tempScriptPath = ".\update_backend.sh"
    $remoteScriptPath = "/home/ubuntu/update_backend.sh"

    # Ensure Unix line endings
    $scriptContent = $scriptContent -replace "`r`n", "`n"

    Set-Content -Path $tempScriptPath -Value $scriptContent -NoNewline

    scp $tempScriptPath ubuntu@${InstanceIp}:$remoteScriptPath
    ssh ubuntu@${InstanceIp} "chmod +x $remoteScriptPath && sudo $remoteScriptPath"
    Remove-Item -Path $tempScriptPath
}

Set-Location -Path ..\terraform

if (-not $InstanceIp) {
    Write-Host "Available instances:"
    List-Instances
    Set-Location -Path ..\scripts
    exit
}

Update-Backend -InstanceIp $InstanceIp -CommitSha $CommitSha

Set-Location -Path ..\scripts
