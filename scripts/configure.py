import json
import os
import sys
import argparse
import subprocess
import yaml

DEFAULT_LINUX_ssh_key_path = os.path.expanduser("~/.ssh/id_rsa.pub")
DEFAULT_WINDOWS_ssh_key_path = os.path.expandvars("%USERPROFILE%\\.ssh\\id_rsa.pub")

DEFAULT_LINUX_YC_TOKEN_PATH = os.path.expanduser("~/.config/yandex-cloud/keys/yc-token")


def get_default_ssh_key_path():
    if os.name == 'nt':
        return DEFAULT_WINDOWS_ssh_key_path
    else:
        return DEFAULT_LINUX_ssh_key_path


def get_default_yc_token_path():
    if os.name == 'nt':
        return ""
    else:
        return DEFAULT_LINUX_YC_TOKEN_PATH


def read_file(file_path):
    try:
        with open(file_path, 'r') as file:
            return file.read().strip()
    except Exception as e:
        return None


def get_yc_config():
    result = subprocess.run(['yc', 'config', 'list'], capture_output=True, text=True)
    if result.returncode != 0:
        return None
    return result.stdout


def get_yc_service_account_key():
    result = subprocess.run(['yc', 'config', 'get', 'service-account-key', '--format', 'json'], capture_output=True, text=True)
    if result.returncode != 0:
        return None
    return result.stdout


def get_yc_static_keys(static_key_path):
    try:
        with open(static_key_path) as f:
            lines = f.readlines()
            return lines[0].strip(), lines[1].strip()
    except:
        return None, None


def parse_yc_config(yc_config):
    config_lines = yc_config.splitlines()
    config = {}
    for line in config_lines:
        if ':' in line:
            key, value = line.split(':', 1)
            config[key.strip()] = value.strip()
    config["service_account_key_file"] = get_yc_service_account_key()
    return config


def create_sensitive_file(ssh_public_key_content, service_account_key_file, static_key_pub, static_key_secret, folder_id, cloud_id, service_account_id):
    sensitive_file_content = f"""ssh_public_key = "{ssh_public_key_content}"

service_account_key_file = <<EOF
{service_account_key_file.strip()}
EOF

static_key_pub = "{static_key_pub}"
static_key_secret = "{static_key_secret}"
folder_id = "{folder_id}"
cloud_id = "{cloud_id}"
service_account_id = "{service_account_id}"
"""

    with open("../terraform/sensitive.tfvars", "w") as sensitive_file:
        sensitive_file.write(sensitive_file_content)
    print("sensitive.tfvars file created successfully.")

def main():
    if len(sys.argv) > 2:
        parser = argparse.ArgumentParser(description="Create sensitive.tfvars for Terraform.")
        parser.add_argument('-s', '--ssh-key-path', type=str, help="Path to the ssh RSA public key (vm access).")
        parser.add_argument('-k', '--yc-static-key-path', type=str, help="Path to the static yc key (storage access).")
        args = parser.parse_args()

        ssh_key_path = args.ssh_key_path or get_default_ssh_key_path()
        if not ssh_key_path.endswith(".pub"):
            print(f"Invalid ssh key path {ssh_key_path}. It should end with .pub.")
        public_ssh_key_content = read_file(ssh_key_path)
        if not public_ssh_key_content:
            print(f"Invalid ssh key path {ssh_key_path}.")
            sys.exit(1)
        yc_static_key_path = args.yc_static_key_path
        if not yc_static_key_path:
            print(f"Invalid yc static key path {yc_static_key_path}.")
            sys.exit(1)
        static_key_pub, static_key_secret = get_yc_static_keys(yc_static_key_path)
        if not static_key_pub or not static_key_secret:
            print(f"Invalid yc static key path {yc_static_key_path} or file format")
            sys.exit(1)
        yc_config = get_yc_config()
        if not yc_config:
            print("Error: Unable to get yc config. Please make sure Yandex Cloud CLI is configured properly.")
            sys.exit(1)
    else:
        while True:
            ssh_key_path = input(f"Enter path to RSA public key for vm access (enter for default value: {get_default_ssh_key_path()}): ") or get_default_ssh_key_path()
            if not ssh_key_path.endswith(".pub"):
                print(f"Invalid ssh key path {ssh_key_path}. It should end with .pub.")
                continue
            public_ssh_key_content = read_file(ssh_key_path)
            if public_ssh_key_content:
                break
            else:
                print(f"Invalid ssh key path {ssh_key_path}. Please try again.")
        while True:
            yc_static_key_path = input(f"Enter path to yc static key: ")
            if not yc_static_key_path:
                print(f"Invalid yc static key path {yc_static_key_path}.")
                continue
            static_key_pub, static_key_secret = get_yc_static_keys(yc_static_key_path)
            if not static_key_pub or not static_key_secret:
                print(f"Invalid yc static key path {yc_static_key_path} or file format")
            break

        yc_config = get_yc_config()
        if not yc_config:
            print("Error: Unable to get yc config. Please make sure Yandex Cloud CLI is configured properly.")
            sys.exit(1)

    config = parse_yc_config(yc_config)
    folder_id = config.get('folder-id')
    cloud_id = config.get('cloud-id')
    service_account_id = config.get('service_account_id')
    service_account_key_content = config.get('service_account_key_file')

    flag = False
    if not folder_id:
        print("folder-id is not specified, call \"yc config profile\" first")
        flag = True
    if not cloud_id:
        print("cloud-id is not specified, call \"yc config profile\" first")
        flag = True
    if not service_account_id:
        flag = True
        print("service_account_id is not specified, call \" yc config profile\" first")
    if not service_account_key_content:
        flag = True
        print("service_account_key_content is not specified, call \" yc config profile\" first")
    if flag:
        sys.exit(1)

    create_sensitive_file(public_ssh_key_content, service_account_key_content, static_key_pub, static_key_secret, folder_id, cloud_id, service_account_id)


if __name__ == "__main__":
    main()
